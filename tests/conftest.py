# coding: utf-8

import pytest


from backports.surrogateescape import register_handler


@pytest.fixture(scope="session")
def handler_name():
    """Fixture to register the handler for the tests."""

    name = "backports.surrogateescape"
    register_handler(name)
    return name
