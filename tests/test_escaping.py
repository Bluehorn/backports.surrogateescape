# coding: utf-8

import codecs
import struct

import pytest
from hypothesis import given, settings, example
from hypothesis import strategies as st

from backports.surrogateescape.handler import encode, decode


def has_builtin_surrogateescape():
    try:
        codecs.lookup_error("surrogateescape")
    except LookupError:
        return False
    else:
        return True


def test_trivial(handler_name):
    """Converts a simple (non UTF-8) bytestring to unicode and back."""
    binary = b"First \x81\x82\xe4 example"
    text = binary.decode("utf-8", handler_name)
    assert encode(text) == binary


def test_roundtrip(handler_name):
    _test_roundtrip(handler_name)


@given(st.binary())
@settings(max_examples=10000)
@example(b'\xed\xb2\x80')
def _test_roundtrip(handler_name, binary):
    assert encode(decode(binary, handler_name)) == binary


@pytest.mark.skipif(not has_builtin_surrogateescape(), reason="No builtin surrogateescape handler")
def test_all_bytes(handler_name):
    for value in range(256):
        binary = struct.pack("B", value)
        assert binary.decode("utf-8", handler_name) == binary.decode("utf-8", "surrogateescape")


@pytest.mark.skipif(not has_builtin_surrogateescape(), reason="No builtin surrogateescape handler")
def test_decode_like_python3(handler_name):
    _test_decode_like_python3(handler_name)


@given(st.binary())
@settings(max_examples=10000)
def _test_decode_like_python3(handler_name, binary):
    assert decode(binary, handler_name) == binary.decode("utf-8", "surrogateescape")


@pytest.mark.skipif(not has_builtin_surrogateescape(), reason="No builtin surrogateescape handler")
@given(st.text())
@settings(max_examples=10000)
def test_encode_like_python3(text):
    assert encode(text) == text.encode("utf-8", "surrogateescape")
