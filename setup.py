# coding: utf-8

from setuptools import setup

setup(
    name="backports.surrogateescape",
    version="0.1",
    author="Torsten Landschoff",
    author_email="torsten@landschoff.net",
    packages=["backports", "backports.surrogateescape"],
    description="surrogateescape encoding error handler for Python 2",
    classifiers=[
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: MIT License",
    ],
    extras_require={
        "test": ["pytest", "hypothesis"],
    }
)
