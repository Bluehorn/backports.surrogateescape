# coding: utf-8

import functools
import re
import struct


try:
    _make_unichar = unichr
    _make_byte = chr
    _byte_value = ord
except NameError:
    _make_unichar = chr
    _make_byte = functools.partial(struct.pack, "B")
    _byte_value = lambda x: x


def error_handler(error):
    if isinstance(error, UnicodeDecodeError):
        bad_byte = _byte_value(error.object[error.start])
        if 0x80 <= bad_byte <= 0xff:
            escape = 0xdc00 + bad_byte
            return _make_unichar(escape), error.start + 1

    raise error


_BAD_CONTINUATION_RE = re.compile(b"\xed(?=[\xb2\xb3][\x80-\xbf])")


def decode(binary, handler_name):
    parts = []
    last = 0
    for match in _BAD_CONTINUATION_RE.finditer(binary):
        parts.append(binary[last:match.start()].decode("utf-8", handler_name))
        parts.append(_make_unichar(0xdc00 + _byte_value(binary[match.start()])))
        last = match.end()

    if last:
        parts.append(binary[last:].decode("utf-8", handler_name))
        return u"".join(parts)
    else:
        return binary.decode("utf-8", handler_name)


_SURROGATES_RE = re.compile(u"[\udc80-\udcff]")


def encode(text):
    buf = []
    last = 0
    for match in _SURROGATES_RE.finditer(text):
        buf.append(text[last:match.start()].encode("utf-8"))
        buf.append(_make_byte(ord(text[match.start()]) - 0xdc00))
        last = match.end()

    if last:
        buf.append(text[last:].encode("utf-8"))
        return b"".join(buf)
    else:
        return text.encode("utf-8")
