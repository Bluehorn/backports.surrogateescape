# coding: utf-8

import codecs
from .handler import error_handler


def register_handler(name="surrogateescape"):
    codecs.register_error(name, error_handler)
